# ansible-role-graylog_collector_sidecar

Ansible role - Install Graylog Collector Sidecar on production server to receive logs

## Usage

### Download role with Ansible Galaxy

#### - Directly in command line

 ``` bash
 # Command line
 ansible-galaxy install git+https://github.com/e-COSI/ansible-role-graylog_collector_sidecar.git
 ```

#### - With a "requirements.yml" file

``` yaml
# requirements.yml
---
- src: ssh://git@github.com/e-COSI/ansible-role-graylog_collector_sidecar.git
  version: master
  name: graylog_collector_sidecar
  scm: git
```

``` bash
# Command line
ansible-galaxy install -r requirement.yml
```

## Role Variables

|Variable Name| Required | Default Value | Description |
|-------------|----------|---------------|-------------|
|graylog_collector_sidecar_version|Yes|0.1.5|Version of Collector Sidecar to install|
|graylog_collector_sidecar_github_repo|Yes| [https://github.com/Graylog2/collector-sidecar](https://github.com/Graylog2/collector-sidecar)|Repository github where download Collector-Sidecar|
|graylog_collector_sidecar_config_file_path|Yes|/etc/graylog/collector-sidecar/|Path where configuration files will be saved|
|graylog_collector_sidecar_config_file_name|Yes|collector_sidecar.yml|Name of configuration file|
|graylog_collector_sidecar_config_fragmented_log_directory|Yes|fragmented_log_files| Directory where are merged some configuration files|
|graylog_collector_sidecar_config_fragmented_log_file_name_full_path|Yes|"{{ graylog_collector_sidecar_config_file_path }}/log_lists/fragmented_log_files"| Name of merged configuration file|
|graylog_collector_sidecar_config_fragmented_log_file_name|Yes|"fragmented_log_files-{{ inventory_hostname }}"|Name of fragmented configuration files|
|graylog_collector_sidecar_config_fragmented_tag_file_name_full_path|Yes|"{{ graylog_collector_sidecar_config_file_path }}/tags/fragmented_tags"| Name of merged configuration file|
|graylog_collector_sidecar_config_fragmented_tag_file_name|Yes|"fragmented_tags-{{ inventory_hostname }}"|Name of fragmented configuration files|
|graylog_collector_sidecar_server_url|Yes|<http://127.0.0.1:9000/api/>|URL of Graylog server API where logs are sent|
|graylog_collector_sidecar_update_interval|Yes|10|
|graylog_collector_sidecar_tls_skip_verify|Yes|false|
|graylog_collector_sidecar_send_status|Yes|true|
|graylog_collector_sidecar_list_log_files|Yes|<ul><li>/var/log</li></ul>|List of log files to send|
|graylog_collector_sidecar_collector_id|Yes|file:/etc/graylog/collector-sidecar/collector-id|File where are saved Collector-Sidecar id|
|graylog_collector_sidecar_cache_path|Yes|/var/cache/graylog/collector-sidecar|Collector-Sidecar cache path|
|graylog_collector_sidecar_log_path|Yes|/var/log/graylog/collector-sidecar|Collector-Sidecar log path|
|graylog_collector_sidecar_log_rotation_time|Yes|86400|Rotation time for Collector Sidecar log files|
|graylog_collector_sidecar_log_max_age|Yes|604800|Retention of Collector-Sidecar log files|
|graylog_collector_sidecar_tags|Yes|['all']|Tags associate to Collector-Sidecar|
|graylog_collector_sidecar_backends|Yes|<ul><li>{name: nxlog, enabled: false, binary_path: /usr/bin/nxlog, configuration_path: /etc/graylog/collector-sidecar/generated/nxlog.conf}</li><li>{name: filebeat, enabled: true, binary_path: /usr/bin/filebeat, configuration_path: /etc/graylog/collector-sidecar/generated/filebeat.yml}</li></ul>|Backend application sending log messages|